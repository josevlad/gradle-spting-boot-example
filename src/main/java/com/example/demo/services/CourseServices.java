package com.example.demo.services;

import com.example.demo.component.BusinessLogicExceptionComponent;
import com.example.demo.model.dto.CourseDTO;
import com.example.demo.model.entity.Course;
import com.example.demo.model.mapper.CourseMapper;
import com.example.demo.model.mapper.CycleAvoidingMappingContext;
import com.example.demo.model.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServices {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CycleAvoidingMappingContext context;

    @Autowired
    private BusinessLogicExceptionComponent exceptionComponent;

    private CourseMapper courseMapper = CourseMapper.MAPPER;

    public CourseDTO save(CourseDTO dto) {
        // 1) consultar en la base de datos si existe un curso con el codigo que tiene el dto [ findByCoce() ],
        // si existe, tira una Exception que esta definida en BusinessLogicExceptionComponent y se da la respuesta
        // de tipo 409 CONFLICT
        courseRepository
                .findByCode(dto.getCode())
                .ifPresent(course -> {
                    throw exceptionComponent.throwExceptionEntityAlreadyExists(
                            "Course",
                            String.format("code '%s'", course.getCode())
                    );
                });

        // 2) si no exite, se convierte el dto a entity y se guarda en una variable de ese tipo
        Course courseToSave = courseMapper.toEntity(dto, context);

        // 3) se le inidica al repository que guarde esa variable de tipo entity en la base de datos [ save() ]
        Course savedEntity = courseRepository.save(courseToSave);

        // 4) se convierte la la variable de tipo entity, guardada en la DB, a dto
        CourseDTO savedDto = courseMapper.toDto(savedEntity, context);

        // 5) se retorna ese dto convertido desde entity
        return savedDto;
    }

}
