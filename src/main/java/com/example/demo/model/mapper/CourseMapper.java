package com.example.demo.model.mapper;

import com.example.demo.model.dto.CourseDTO;
import com.example.demo.model.entity.Course;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CourseMapper extends DataCycleMapper<CourseDTO, Course> {
    CourseMapper MAPPER = Mappers.getMapper(CourseMapper.class);
}
