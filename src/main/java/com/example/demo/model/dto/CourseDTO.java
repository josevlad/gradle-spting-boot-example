package com.example.demo.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter @Setter
@NoArgsConstructor
public class CourseDTO implements Serializable {
    private Long id;

    @NotBlank(message = "name is required")
    private String name;

    @NotBlank(message = "code is required")
    private String code;

    @NotNull(message = "price is required")
    private BigDecimal price;

    @NotBlank(message = "description is required")
    private String description;
}

/*
{
    "name": "java",
    "code": "jvm-5023",
    "price": 100000,
    "description": "aprende en 5 meses java"
}
*/
