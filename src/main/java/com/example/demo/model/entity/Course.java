package com.example.demo.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "course")
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(nullable = false, length = 10, unique = true)
    private String code;

    @NotNull
    @Column(name = "price", precision = 10, scale = 2)
    private BigDecimal price;

    @NotNull
    @Column(name = "description")
    private String description;

    /*********************
     * Getter and Setter *
     *********************/

    public Course setName(String name) {
        this.name = name;
        return this;
    }

    public Course setCode(String code) {
        this.code = code;
        return this;
    }

    public Course setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Course setDescription(String description) {
        this.description = description;
        return this;
    }
}
