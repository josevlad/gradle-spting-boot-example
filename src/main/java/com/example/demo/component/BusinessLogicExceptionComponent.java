package com.example.demo.component;

import com.example.demo.exception.ApiEntityError;
import com.example.demo.exception.BusinessLogicException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class BusinessLogicExceptionComponent {

    public void throwExceptionEntityNotFound(String entityName, Long id) {
        ApiEntityError apiEntityError = new ApiEntityError(
                entityName,
                "NotFound",
                String.format("The %s with id '%d' does not exist", entityName, id)
        );

        throw new BusinessLogicException(
                entityName + " does not exist",
                HttpStatus.NOT_FOUND,
                apiEntityError
        );
    }

    public RuntimeException throwExceptionEntityAlreadyExists(String entityName, Long id) {
        ApiEntityError apiEntityError = new ApiEntityError(
                entityName,
                "AlreadyExists",
                String.format("The %s with id '%d' does not exist", entityName, id)
        );

        return new BusinessLogicException(
                entityName + " already exists",
                HttpStatus.CONFLICT,
                apiEntityError
        );
    }

    public RuntimeException throwExceptionEntityAlreadyExists(String entityName, String str) {
        ApiEntityError apiEntityError = new ApiEntityError(
                entityName,
                "AlreadyExists",
                String.format("The %s with %s already exists", entityName, str)
        );

        return new BusinessLogicException(
                entityName + " already exists",
                HttpStatus.CONFLICT,
                apiEntityError
        );
    }
}
