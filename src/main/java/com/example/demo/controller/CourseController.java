package com.example.demo.controller;

import com.example.demo.model.dto.CourseDTO;
import com.example.demo.services.CourseServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseServices courseServices;

    @PostMapping({ "", "/" })
    public ResponseEntity newCourse(@RequestBody @Valid CourseDTO dto) {
        CourseDTO saved = courseServices.save(dto);
        return ResponseEntity.ok().body(saved);
    }
}
