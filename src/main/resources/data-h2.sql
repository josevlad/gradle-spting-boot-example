INSERT INTO course
    ( id, name, code, description, price )
VALUES
    ( 1, 'JAVA', 'jvm-985', 'java description', 100000),
    ( 2, 'PYTHON', 'py-4801', 'python description', 90000),
    ( 3, 'JAVASCRIPT', 'js-7591', 'js description', 90000),
    ( 4, 'REAC', 'rc-8573', 'react description', 80000),
    ( 5, 'ANGULAR', 'ang-6582', 'angular description', 80000);